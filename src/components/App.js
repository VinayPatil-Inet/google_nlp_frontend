import React, {  useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route,Redirect } from 'react-router-dom'
import ResetPassword from './ResetPassword'
import HealthNLPAPI from './HealthNLPAPI'
import ForgotPassword from './ForgotPassword'
import Navbar from './NavBar';
import FileViewList from './FileViewList'
import Login from './Login'




function App() {

  return (

<div id="container" className='d-flex'>


      <div className='w-100'>

        <Router>


          <Switch>
          <Route exact path='/' component={Login} />

            {/* <Route exact path='/' component={HealthNLPAPI} /> */}

             <Route exact path='/getData' component={HealthNLPAPI} />
             <Route exact path='/login' component={Login} />
             <Route exact path='/fileView/:id' component={FileViewList} />
             <Route exact path='/ForgotPassword' component={ForgotPassword} />
             <Route exact path='/ResetPassword' component={ResetPassword} />







            <div>


           <Navbar />








            </div>

          </Switch>

        </Router>
        {/*
        <Router>
        <Route exact path='/userlogin' component={Userlogin} />
          <Navbar />
          <Switch>
           /* <Route path='/MyPayerList' component={MyPayerList} /> *
            <Route path='/ticketRequest' component={TicketRequest} />
            <Route path='/Billing' component={Billing} />
            <Route path='/form' component={FormFill} />
            <Route path='/MyPayerList/:id' component={MyPayerList} />
            <Route path='/MyPayerList' component={MyPayerList} />
            <Route path='/newPayer' component={NewPayer} />
            <Route path='/viewPayer/:id' component={ViewPayer} />
            <Route path='/BillingDetails/:id' component={BillingDetails} />
            <Route path='/Contract' component={Contract} />
            <Route path='/PayersRegistered' component={PayersRegistered} />
            <Route path='/reports' component={Reports} />
            <Route path='/managerDashboard' component={ManagerDashboard} />
            <Route path='/payerContactList' component={PayerContactList} />
            <Route path='/searchPayer' component={SearchPayer} />
            <Route path='/editPayerContact' component={UpdatePayerContact} />
            <Route path='/addPayerContact' component={AddPayerContact} />
            <Route path='/newOrganization' component={NewOrganization} />
            <Route path='/signup' component={Signup} />


          </Switch>

        </Router> */}

        {/* <Router>
          <AuthProvider>
            <Switch>
              <PrivateRoute exact path='/' component={Dashboard} />
              <Route path='/signup' component={Signup} />
              <Route path='/Home' component={Navbar} />

              <Route path='/userlogin' component={Userlogin} />
              <Route path='/form' component={FormFill} />
              <Route path='/MyPayerList/:id' component={MyPayerList} />
              <Route path='/MyPayerList' component={MyPayerList} />
              <Route path='/newPayer' component={NewPayer} />
              <Route path='/viewPayer/:id' component={ViewPayer} />

              <Route path='/ticketRequest' component={TicketRequest} />
              <Route path='/Billing' component={Billing} />
              <Route path='/BillingDetails/:id' component={BillingDetails} />
              <Route path='/Contract' component={Contract} />
              <Route path='/PayersRegistered' component={PayersRegistered} />
              <Route path='/reports' component={Reports} />
              <Route path='/managerDashboard' component={ManagerDashboard} />
              <Route path='/payerContactList' component={PayerContactList} />
              <Route path='/searchPayer' component={SearchPayer} />
              <Route path='/editPayerContact' component={UpdatePayerContact} />
              <Route path='/addPayerContact' component={AddPayerContact} />
              <Route path='/newOrganization' component={NewOrganization} />

            </Switch>
          </AuthProvider>
        </Router> */}
      </div>
    {/* </Container> */}
    </div>
  );
}
{/* <Route path='/viewPayer' component={ViewPayer} /> */ }
{/* <Route path='/login' component={Login} /> */ }
export default App;