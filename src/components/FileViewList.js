import React, { useEffect, useRef, useState } from 'react';
import { Container, Row, Col, Form, Button, Card, InputGroup } from "react-bootstrap";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import axios from 'axios';
import Img from './BCBSRI-LOGO.png'
import { Link, useHistory, useParams } from 'react-router-dom';
import BootstrapTable from "react-bootstrap-table-next";
import { ExportToExcel } from '../ExportToExcel'
import ToolkitProvider, { CSVExport } from 'react-bootstrap-table2-toolkit';
import Highlighter from "react-highlight-words";

import { CSVLink, CSVDownload } from 'react-csv';



const FileViewList = () => {
  const { id } = useParams();
  const fileName = "mysamplefile";
  const { ExportCSVButton } = CSVExport;
  const [text, setText] = useState([]);
  const [fileList, setFileList] = useState({
    entities: [], context: [], relation: [], fulljson: [], contentArrays: [],
    filteredEntityLNC: [],
    filteredEntitySnomed: [],
    allCodes: []
    // contentLNCcodes: [], contentSnomedcodes: []
  });


  useEffect(() => {
    var padma = sessionStorage.getItem('count')
    console.log(padma, "padma")
    axios
      .get(process.env.REACT_APP_BASE_URL + `/api/getIdByFile/${id}`)
      .then((res) => {
        console.log(res.data.data[0], "getIdByUser")
        // var final_json_res = res.data.data[0].response_json;
        // var resultEntities = final_json_res.entities;
        // var resultRelation = final_json_res.relationships;
        // var resultContext = final_json_res.entityMentions;
        // console.log(resultEntities, "resultEntities")
        // var filteredEntity = [];
        // resultEntities.map(filteredName => {
        //     var filteredpnames;
        //     var filteredICD = [];
        //     filteredName.vocabularyCodes.filter(name => name.includes('ICD10CM')).map(filteredNames => {
        //         // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
        //         filteredpnames = filteredName.preferredTerm
        //         filteredICD.push(filteredNames)
        //     })
        //     if (filteredpnames !== undefined) {
        //         filteredEntity.push({ "preferredTerm": filteredpnames, "ICDCode": filteredICD })
        //         console.log(filteredICD, "filteredICD")
        //     }
        // })

        var final_json_res = res.data.data[0].response_json;
        var resultEntities = final_json_res.entities;

        var resultRelation = final_json_res.relationships;
        var resultContext = final_json_res.entityMentions;
        var filteredEntity = [];
        var filteredEntityall = [];
        var filteredEntityLNC = []
        var filteredEntitySnomed = []
        var contentArray = [];
        var allCodes = []
        // var contentLNCcode = [];
        // var contentSnomedcode = []

        resultEntities.map(filteredName => {
          var filteredpnames;
          var filteredICD = [];
          filteredName.vocabularyCodes.filter(name => name.includes("ICD10")).map(filteredNames => {
            // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
            filteredpnames = filteredName.preferredTerm

            filteredICD.push(filteredNames)

          })
          if (filteredpnames !== undefined) {
            filteredEntity.push({ "preferredTerm": filteredpnames, "ICDCode": filteredICD })
            resultContext.map(entityMention => {
              //  console.log(entityMention.linkedEntities, "entityMention.text.content")
              if (entityMention.linkedEntities !== undefined) {
                entityMention.linkedEntities.map(linkedEntity => {
                  // console.log(linkedEntity.entityId, filteredName.entityId, "linkedEntity.entityId")
                  if (filteredName.entityId === linkedEntity.entityId)
                    contentArray.push(entityMention.text.content);
                })
              }
            })
          }
        })
        // //loinc code
        resultEntities.map(filteredNameLNC => {
          var filteredpnamesLNC;
          var filteredLNC = [];
          filteredNameLNC.vocabularyCodes.filter(name => name.includes("LNC")).map(filteredNamesLNC => {
            // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
            filteredpnamesLNC = filteredNameLNC.preferredTerm
            filteredLNC.push(filteredNamesLNC)
            // console.log(filteredNamesLNC.length,"LNC")
          })
          if (filteredpnamesLNC !== undefined) {
            filteredEntityLNC.push({ "preferredTermLNC": filteredpnamesLNC, "LNCCode": filteredLNC })
            console.log(filteredLNC, "filteredLNC")
            resultContext.map(entityMention => {
              //  console.log(entityMention.linkedEntities, "entityMention.text.content")
              if (entityMention.linkedEntities !== undefined) {
                entityMention.linkedEntities.map(linkedEntity => {
                  //    console.log(linkedEntity.entityId, filteredName.entityId, "linkedEntity.entityId")
                  if (filteredNameLNC.entityId === linkedEntity.entityId)
                    contentArray.push(entityMention.text.content);
                })
              }
            })
          }
        })
        // //Snomed code
        resultEntities.map(filteredNameSnomed => {
          var filteredpnamesSnomed;
          var filteredSnomed = [];
          filteredNameSnomed.vocabularyCodes.filter(name => name.includes("SNOMEDCT")).map(filteredNamesSnomed => {
            // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
            filteredpnamesSnomed = filteredNameSnomed.preferredTerm
            filteredSnomed.push(filteredNamesSnomed)
            // console.log(filteredNamesLNC.length,"LNC")
          })
          if (filteredpnamesSnomed !== undefined) {
            filteredEntitySnomed.push({ "preferredTermSnomed": filteredpnamesSnomed, "SnomedCode": filteredSnomed })
            console.log(filteredSnomed, "filteredSnomed")
            resultContext.map(entityMention => {
              //  console.log(entityMention.linkedEntities, "entityMention.text.content")
              if (entityMention.linkedEntities !== undefined) {
                entityMention.linkedEntities.map(linkedEntity => {
                  //    console.log(linkedEntity.entityId, filteredName.entityId, "linkedEntity.entityId")
                  if (filteredNameSnomed.entityId === linkedEntity.entityId)
                    contentArray.push(entityMention.text.content);


                })
              }
            })
          }
        })

        //All codes export csv
        resultEntities.map(filteredName => {
          var filteredpnames;
          var filteredICD = [];
          filteredName.vocabularyCodes.filter(name => name.includes("ICD10")).map(filteredNames => {
            filteredpnames = filteredName.preferredTerm
            filteredICD.push(filteredNames)
          })
          if (filteredpnames !== undefined) {
            filteredEntityall.push({ "preferredTerm": filteredpnames,"CodeTypeICD10": "ICD10",  "ICDCode": filteredICD })
          }
        })
        // //loinc code
        resultEntities.map(filteredNameLNC => {
          var filteredpnamesLNC;
          var filteredLNC = [];
          filteredNameLNC.vocabularyCodes.filter(name => name.includes("LNC")).map(filteredNamesLNC => {
            filteredpnamesLNC = filteredNameLNC.preferredTerm
            filteredLNC.push(filteredNamesLNC)
          })
          if (filteredpnamesLNC !== undefined) {
            filteredEntityall.push({ "preferredTermLNC": filteredpnamesLNC, "CodeTypeLIONC": "LIONC", "LNCCode": filteredLNC })
          }
        })
        // //Snomed code
        resultEntities.map(filteredNameSnomed => {
          var filteredpnamesSnomed;
          var filteredSnomed = [];
          filteredNameSnomed.vocabularyCodes.filter(name => name.includes("SNOMEDCT")).map(filteredNamesSnomed => {
            filteredpnamesSnomed = filteredNameSnomed.preferredTerm
            filteredSnomed.push(filteredNamesSnomed)
          })
          if (filteredpnamesSnomed !== undefined) {
            filteredEntityall.push({ "preferredTermSnomed": filteredpnamesSnomed, "CodeTypeSNOMED": "SNOMED", "SnomedCode": filteredSnomed })
          }
        })
        allCodes.push({
          filteredEntityall: filteredEntityall,
        })
        console.log(filteredEntityall, "filteredEntityall")
        var data = []
        allCodes.forEach(item => {
          for (var i = 0; i < item.filteredEntityall.length; i++) {
            const role = item.filteredEntityall[i]
            data.push({
              CodeType: role.CodeTypeICD10 || role.CodeTypeLIONC  || role.CodeTypeSNOMED ,
              preferredTerm: role.preferredTerm || '' || role.preferredTermLNC || role.preferredTermSnomed,
              ICDCode: role.ICDCode || role.LNCCode || role.SnomedCode,
            });
          }
        });

        setText(res.data.data[0].text)
        setFileList({
          ...fileList, entities: filteredEntity, context: resultContext, relation: resultRelation,
          fulljson: final_json_res, contentArrays: contentArray,
          filteredEntityLNC: filteredEntityLNC,
          filteredEntitySnomed: filteredEntitySnomed,
          allCodes: data
        })
      });
  }, []);




  // const columns = [
  //     { dataField: 'preferredTerm', text: 'Preferred Term' },
  //     { dataField: 'ICDCode', text: 'ICD10 CODE' },
  //     { dataField: 'preferredTermLNC', text: 'Preferred Term' },
  //      { dataField: 'LNCCode', text: 'LNC CODE' },
  // ]

  const columns = [
    { dataField: 'preferredTerm', text: 'Preferred Term' },
    { dataField: 'ICDCode', text: 'ICD10 CODE' },

  ]
  const columns_LNC = [
    { dataField: 'preferredTermLNC', text: 'Preferred Term' },
    { dataField: 'LNCCode', text: 'LONIC CODE' },
  ]

  const columns_Snomed = [
    { dataField: 'preferredTermSnomed', text: 'Preferred Term' },
    { dataField: 'SnomedCode', text: 'SNOMED CODE' },
  ]

  return (
    <Card style={{ minHeight: "70px", width: "70%", marginLeft: "13%", marginTop: "1%" }}>
      {/* <img src={Img} alt="pic" width={200} style={{ marginLeft: "5%" }} />
            <h3 className='text-center  heading'  >File Details</h3>
            <Link to={`/getData`}>
                <button
                    className="btn btn-primary  btn-xs" Width="95px"
                    style={{ marginLeft: "80%", borderRadius: "20px" ,}}
                >
                    Back
                </button>
            </Link> */}

      <div className="col-md-12">

        <h3 className='text-center heading' >File Details</h3>



        <img src={Img} alt="pic" width={200} />
        <div className="pull-right">
          <Link to={`/getData`}>
            <button
              className="btn btn-primary text-right btn-xs" Width="50px"
              style={{ borderRadius: "20px", marginTop: "45%" }}
            >
              Back
            </button>
          </Link>
        </div>

      </div>

      <hr></hr>
      <Card.Body>
        <Row>
          <Col>
            <Card>
              <h3 className='text-center  heading'>Inputed Text</h3>

              <Highlighter highlightStyle={{ backgroundColor: 'yellow' }}
                highlightClassName="YourHighlightClass"
                searchWords={fileList.contentArrays
                  //  &&
                  // fileList.contentLNCcodes &&
                  // fileList.contentSnomedcodes
                }

                autoEscape={true}
                textToHighlight={text}
              />
              {/* {text} */}
            </Card>
            <br></br>
            <Card>
              <h3 className='text-center  heading'>Resulted Data</h3>


              {/* data={fileList.entities}
                                columns={columns} */}

              <CSVLink data={fileList.allCodes}

                filename={"All_Codes.csv"}
                className="btn btn-success btn-xs" style={{ marginLeft: "85%",marginRight:"2%" }}
              >
               Export CSV!!
              </CSVLink>


              <Tabs defaultActiveKey="first" style={{ marginLeft: "3%" }}>
                <Tab eventKey="first" title="ICD10 Codes"  >
                  <br></br>
                  <ToolkitProvider
                    keyField="id"
                    data={fileList.entities}
                    columns={columns}
                    exportCSV
                  >
                    {
                      props => (
                        <div>
                          {/* <ExportCSVButton {...props.csvProps}
                                                className="btn btn-success btn-xs"
                                                style={{ marginLeft: "85%" }}
                                            >Export CSV!!</ExportCSVButton> */}
                          <hr />
                          <BootstrapTable {...props.baseProps} />
                        </div>
                      )
                    }
                  </ToolkitProvider>

                  {/* <div>
                  {fileList.entities.map(x => {
                  const { ICDCode } = x;
                  if (ICDCode != null) {
                    return (
                      <div>
                        <Card >
                        <p><b>{x.preferredTerm} - {x.ICDCode}</b></p>

                        </Card>
                      </div>
                    )

                  }
                }
                )}
              </div> */}



                </Tab>
                <Tab eventKey="second" title="LOINC Codes">
                  <br></br>

                  <div>

                    <ToolkitProvider
                      keyField="id"
                      data={fileList.filteredEntityLNC}
                      columns={columns_LNC}
                      exportCSV
                    >
                      {
                        props => (
                          <div>
                            {/* <ExportCSVButton {...props.csvProps}
                                                className="btn btn-success btn-xs"
                                                style={{ marginLeft: "85%" }}
                                            >Export CSV!!</ExportCSVButton> */}
                            <hr />
                            <BootstrapTable {...props.baseProps} />
                          </div>
                        )
                      }
                    </ToolkitProvider>
                    {/* {fileList.entities.map(x => {
                  const { LNCCode } = x;
                  if (LNCCode != null) {
                    return (
                      <div>
                        <Card >
                        <p><b>{x.preferredTermLNC} - {x.LNCCode}</b></p>
                        </Card>
                      </div>
                    )

                  }
                }
                )} */}
                    {/* {fileList.entities.map(obj => {
                  return (
                    <div>
                      <p><b>{obj.preferredTerm} - {obj.LNCCode}</b></p>
                    </div>
                  );
                })} */}
                  </div>


                </Tab>
                <Tab eventKey="third" title="SNOMED Codes">
                  <br></br>

                  <br></br>

                  <div>
                    <ToolkitProvider
                      keyField="id"
                      data={fileList.filteredEntitySnomed}
                      columns={columns_Snomed}
                      exportCSV
                    >
                      {
                        props => (
                          <div>
                            {/* <ExportCSVButton {...props.csvProps}
                                                className="btn btn-success btn-xs"
                                                style={{ marginLeft: "85%" }}
                                            >Export CSV!!</ExportCSVButton> */}
                            <hr />
                            <BootstrapTable {...props.baseProps} />
                          </div>
                        )
                      }
                    </ToolkitProvider>


                    {/* {fileList.entities.map(obj => {
                  return (
                    <div>
                      <p><b>{obj.preferredTermSnomed} - {obj.SnomedCode}</b></p>
                    </div>
                  );
                })} */}
                    {/* {fileList.entities.map(x => {
                  const { SnomedCode } = x;
                  if (SnomedCode!=null ) {
                    return (
                      <div>
                        <Card >
                        <p><b>{x.preferredTermSnomed} - {x.SnomedCode}</b></p>
                        </Card>
                      </div>
                    )

                  }
                }
                )} */}

                  </div>

                </Tab>
                {/* <Tab eventKey="fourth" title="Json">
            <br></br>
            <Card >
              <br></br>
              <button style={{ marginLeft: "60%" }}
                type="button" onClick={exportData}>
                Download Json response
              </button>
              <br></br>
              <div>
                <p>
                  <pre>
                    {(JSON.stringify(list.fulljson, null, 2))}
                  </pre>
                </p>
              </div>



            </Card>

          </Tab> */}
              </Tabs>
              {/* </Card.Body> */}




            </Card>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  )
}
export default FileViewList;







// {fileList.entities.map(filteredName => {


//                                     return (
//                                         <div>
//                                             <p><b>{filteredName.preferredTerm}</b></p>
//                                             <li>{filteredName.ICDCode.join(", ")} </li>

//                                         </div>

//                                     );



//                             })}


{/* {filesList.length == 0 && <tr>
                      <td className="text-center" colSpan="4">
                        <b>No data found to display.</b>
                      </td>
                    </tr>} */}





{/* {fileList.map(x => {
                            const {entities,entityId } = x;
                            return (
                                <div >
                                    <p>{entities.entityId}</p>
                                </div>
                            )
                        })
                        } */}
{/* {fileList.map(x => {
                  const { entities} = x;
                  if (entities != null) {
                    return (
                      <div>
                        <Card >

                          <p>Subject - {entities.entityId}</p>

                        </Card>
                      </div>
                    )

                  }
                }
                )} */}










