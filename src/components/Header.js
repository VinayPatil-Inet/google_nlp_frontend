import { Button, Row, Col } from 'react-bootstrap';
import React, { Component } from 'react';

const hstyle = {
  "width": "99%", 
  "textAlign":"center",
  "margin": "6px", 
};


class Header extends Component {
  render() {
    return (
      <div style={hstyle}>
        <h2>This is header component</h2>
      </div>
    );
  }
}
 
export default Header;
