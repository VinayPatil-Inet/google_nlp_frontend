import React, { useEffect, useRef, useState } from 'react';
import { Container, Row, Col, Form, Button, Card, InputGroup } from "react-bootstrap";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Healthcare from './files/data.json'
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import Modal from 'react-bootstrap/Modal'
import { Link, useHistory, useParams } from 'react-router-dom';
import BootstrapTable from "react-bootstrap-table-next";
import { ExportToExcel } from '../ExportToExcel'
import ToolkitProvider, { CSVExport } from 'react-bootstrap-table2-toolkit';
import { CSVLink, CSVDownload } from "react-csv";
import Img from './BCBSRI-LOGO.png'
import fs from 'fs'





const HealthNLPAPI = () => {

  const [list, setList] = useState({ entities: [], context: [], relation: [], fulljson: [], filterFinalEntities: [] });
  const { ExportCSVButton } = CSVExport;
  const [print, setPrint] = useState(false)
  const [tab, setTab] = useState('');
  const [text, setText] = useState('')
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [filesList, setFilesList] = useState([]);

  const history = useHistory();



  useEffect(() => {
    var genarateCsvFile = false
    getFiles(genarateCsvFile)

  }, []);


  function getIdFileList(id) {
    fetch(process.env.REACT_APP_BASE_URL+`/api/getIdByFile/${id}`, {
      method: 'get'

    }).then((result) => {
      result.json().then((resp) => {
        console.log(resp, "padma")
        console.log(resp.data, "padma")
      })
    })
      .catch(error => {
        console.log(error.data)
      });

  }

  function getFiles(genarateCsvFile) {
    console.log(genarateCsvFile,"genarateCsvFile")
    axios.get(process.env.REACT_APP_BASE_URL+`/api/GetAllGoogleHealthAPI/${genarateCsvFile}`).then(res => {
      setFilesList(res.data.data);
      console.log(res.data.data, "get files list");
      var arrayResult = res.data.data
      var filterFinalEntity = [];
      arrayResult.map((x) => {
        var final_res = x.response_json
        console.log(final_res, "final_res");
        var resultEntities = final_res.entities
        console.log(resultEntities, "resultEntities");
        var filteredEntity = [];
        resultEntities.map(filteredName => {
          var filteredpnames;
          var filteredICD = [];
          filteredName.vocabularyCodes.filter(name => name.includes('ICD10CM')).map(filteredNames => {
            // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
            filteredpnames = filteredName.preferredTerm
            filteredICD.push(filteredNames)
          })
          if (filteredpnames !== undefined) {
            filteredEntity.push({ "preferredTerm": filteredpnames, "CodeTypeICD10": "ICD10", "ICDCode": filteredICD })
            console.log(filteredICD, "filteredICD")
          }
        })
        //Loinc Codes
        resultEntities.map(filteredName => {
          var filteredpnamesLNC;
          var filteredLNC= [];
          filteredName.vocabularyCodes.filter(name => name.includes('LNC')).map(filteredNames => {
            // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
            filteredpnamesLNC = filteredName.preferredTerm
            filteredLNC.push(filteredNames)
          })
          if (filteredpnamesLNC !== undefined) {
            filteredEntity.push({ "preferredTermLNC": filteredpnamesLNC, "CodeTypeLIONC": "LIONC",  "LNCCode": filteredLNC })
            console.log(filteredLNC, "filteredLNC")
          }
        })
        //Snomed Codes
        resultEntities.map(filteredName => {
          var filteredpnamesSnomed;
          var filteredSnomed= [];
          filteredName.vocabularyCodes.filter(name => name.includes('SNOMEDCT')).map(filteredNames => {
            // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
            filteredpnamesSnomed = filteredName.preferredTerm
            filteredSnomed.push(filteredNames)
          })
          if (filteredpnamesSnomed !== undefined) {
            filteredEntity.push({ "preferredTermSnomed": filteredpnamesSnomed, "CodeTypeSNOMED": "SNOMED", "SnomedCode": filteredSnomed})
            console.log(filteredSnomed, "filteredSnomed")
          }
        })


        console.log(filteredEntity, "filteredEntity")
        filterFinalEntity.push({ filteredEntity: filteredEntity, file_name: x.file_name,


          //  description: filteredEntity.filter(role => role.ICDCode != 'ICD10'),
          })
         // console.log(filterFinalEntity,"padma")

        //  filterFinalEntity.push(filteredEntity)
        // const data = filterFinalEntity.map(item => ({
        //   name: item.file_name,
        //   description: item.filteredEntity.map(role => role.ICDCode),
        //   suggestedRoles: item.filteredEntity.map(role => role.preferredTerm),
        // }))
        var data = []

        filterFinalEntity.forEach(item => {
          data.push({
            FileName: item.file_name,
         //   CodeType :'ICD10' || 'LNC'

          });
          for (var i = 0; i < item.filteredEntity.length; i++) {
            const role = item.filteredEntity[i];
                data.push({
                  FileName: '',
                  CodeType: role.CodeTypeICD10 || role.CodeTypeLIONC  || role.CodeTypeSNOMED ,
                  preferredTerm: role.preferredTerm  || '' || role.preferredTermLNC || role.preferredTermSnomed ,
                  ICDCode: role.ICDCode || role.LNCCode || role.SnomedCode ,
                })
              }
        });


        // setFilesList({ ...filesList, entities: filterFinalEntity })
        setList({ ...list, filterFinalEntities: data })
      })
      console.log(filterFinalEntity, "filterFinalEntity")
    });

  }

  const googleHealthdata = (e) => {
    setLoading(true);
    setIsError(false);
    var text = Healthcare.text
    e.preventDefault()
    axios.post(process.env.REACT_APP_BASE_URL+`/api/GoogleHealthAPI`, {
      text
    })
      .then(response => {
        var genarateCsvFile = true
        getFiles(genarateCsvFile)
        // window.location.reload();
        console.log(response.data, "google health nlp")

        var final_json_res = response.data.data;
        var resultEntities = final_json_res.entities;
        var resultRelation = final_json_res.relationships;
        var resultContext = final_json_res.entityMentions;
        setList({ ...list, entities: resultEntities, context: resultContext, relation: resultRelation, fulljson: final_json_res })
        setLoading(false);
      })
      .catch(error => {
        setLoading(false);
        setIsError(true);
        // console.log(error, "error")
      })



  }



  // const downloadFile = ({ data, fileName, fileType }) => {
  //   const blob = new Blob([data], { type: fileType })

  //   const a = document.createElement('a')
  //   a.download = fileName
  //   a.href = window.URL.createObjectURL(blob)
  //   const clickEvt = new MouseEvent('click', {
  //     view: window,
  //     bubbles: true,
  //     cancelable: true,
  //   })
  //   a.dispatchEvent(clickEvt)
  //   a.remove()
  // }
  // const exportToCsv = e => {
  //   e.preventDefault()

  //   // Headers for each column
  //   let headers = ['FileName,PreferredTerm,ICDCode']

  //   // Convert users data to a csv
  //   let usersCsv = filesList.reduce((acc, user) => {
  //     const { file_name } = user
  //     acc.push([file_name].join(','))
  //     return acc
  //   }, [])

  //   downloadFile({
  //     data: [...headers, ...usersCsv].join('\n'),
  //     fileName: 'users.csv',
  //     fileType: 'text/csv',
  //   })
  // }
  // const exportData = () => {
  //   const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
  //     JSON.stringify(list)
  //   )}`;
  //   const link = document.createElement("a");
  //   link.href = jsonString;
  //   link.download = "data.json";




  //   link.click();
  //   <div class="loader"></div>;
  // };
  const columns = [

    { dataField: 'file_name', text: 'File Name' },
    // { dataField: 'ICDCode', text: 'ICD10 CODE' },
    //  { dataField: 'data.txt', text: 'file Name' },

  ]

  return (
    <Card style={{ minHeight: "70px", width: "60%", marginLeft: "20%", marginTop: "3%" }} >
      <div>
<div className="col-md-12"  >
          <img src={Img} alt="pic" width={200} />
          <div className="pull-right">
          <Link to={`/Login`}>
          <button
            className="btn btn-secondary text-right btn-xs" Width="50px"
            style={{ borderRadius: "20px",marginTop:"45%"}}
          >
            Logout
          </button>
        </Link>


          </div>
          </div>
          <hr></hr>








        {/* <h2 className='text-center mb-4 '>Natural Language Healthcare API Demo</h2> */}

        <form>
          <div class="row">
            <div class="col-sm-4">


              <CSVLink data={list.filterFinalEntities}

              filename={"ICD10_Codes.csv"}
                className="btn btn-info" style={{ marginLeft: "10%", marginTop: "3%" }}
              >
                Download ICD10 CODES
                </CSVLink>

            </div>
            <div class="col-sm-8">
              <Button style={{ marginLeft: '60%', width: '30%', marginTop: "3%" }} disabled={loading}
                onClick={googleHealthdata}
              >{loading ? 'Loading...' : 'Analyse'}</Button>
            </div>
          </div>

          <Card.Body>
            <Row>
              <Col>

                {/* <div>
              {filesList.entities.map(x => {
                  const { file_name,filteredEntity } = x;
                  if (filteredEntity != null) {
                    return (
                      <div>
                        <Card >
                          <p><b>{filteredEntity}</b></p>


                        </Card>
                      </div>
                    )

                  }
                }
                )}
              </div> */}


                {/* {filesList.entities.map(x => {
                  const { filteredEntity,file_name} = x;
                  if (filteredEntity != null) {
                    return (
                      <div>
                        <Card >

                          <p>file_name - {file_name.file_name}</p>

                        </Card>
                      </div>
                    )

                  }
                }
                )} */}

                {/* <ToolkitProvider
                                    keyField="id"
                                    data={filesList.entities}
                                    columns={columns}

                                >
                                    {
                                        props => (
                                            <div>
                                                <ExportCSVButton {...props.csvProps}
                                                   className="btn btn-success btn-xs"
                                                   style={{marginLeft:"85%"}}
                                                >Export CSV!!</ExportCSVButton>
                                                <hr />
                                                <BootstrapTable {...props.baseProps} />
                                            </div>
                                        )
                                    }
                                </ToolkitProvider> */}

                {/* <div className="container-fluid p-3">
                            <BootstrapTable bootstrap4 keyField='id'
                                columns={columns}
                                data={filesList.entities}

                            />
                        </div> */}
                <Table >
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>File Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    {filesList.map((x, index) => <tr>
                      <td>{index + 1}</td>
                      <td>{x.file_name}</td>
                      <td>
                        <Link to={`/fileView/${x.id}`}>
                          <button
                            className="btn btn-success btn-xs" Width="95px"
                            style={{ marginRight: "6%", borderRadius: "20px" }}
                            onClick={() => getIdFileList(x.id)}

                          >
                            View
                          </button>
                        </Link>

                      </td>
                    </tr>)}
                    {filesList.length == 0 && <tr>
                      <td className="text-center" colSpan="4">
                        <b>No data found to display.</b>
                      </td>
                    </tr>}
                  </tbody>
                </Table>


              </Col>
            </Row>


          </Card.Body>




          {/* <div className="row">
            <div className="col-10">
              <Form.Group controlId="pcform.email" className="mb-1">
                <Form.Label>Details</Form.Label>
                <Form.Control style={{ minHeight: "150px" }} as="textarea" rows={3}
                  input class="form-control"
                  value={Healthcare.text}
                  onChange={(e) => { setText(Healthcare.text) }}
                />

              </Form.Group>
            </div>


            <div className="col-2">
              <Form.Label style={{ marginBottom: "70%" }}></Form.Label>
              <Button disabled={loading}
                onClick={googleHealthdata}
                className='w-90 mt-4'>{loading ? 'Loading...' : 'Analyze'}</Button>


            </div>
          </div> */}
        </form>
        <br></br>  <br></br>
        {/*
        <Tabs defaultActiveKey="first" >
          <Tab eventKey="first" title="Knowledge Panel" >
            <br></br>
            <Card>
              <div>
                {list.entities.map(obj => {


                  return (
                    <div>
                      <p><b>{obj.preferredTerm} - {obj.entityId}</b></p>
                      <p>
                        <pre>
                          {(JSON.stringify(obj.vocabularyCodes, null, 2))}
                        </pre>
                      </p>
                    </div>

                  );



                })}
              </div>

            </Card>

          </Tab>
          <Tab eventKey="second" title="Context Assessment">
            <br></br>
            <Card >

              <div>

                {list.context.map(x => {
                  const { type, subject, temporalAssessment, certaintyAssessment, confidence } = x;
                  if (subject != null) {
                    return (
                      <div>
                        <Card >
                          <p><b>{type}</b></p>
                          <p>Subject - {subject.value}</p>
                          <p>Certainty Assessment - {certaintyAssessment.value}</p>
                          <p>Temporal Assessment - {temporalAssessment.value}</p>
                          <p>Overall Confidence - {confidence}</p>

                        </Card>
                      </div>
                    )

                  }
                }
                )}
              </div>
            </Card>

          </Tab>
          <Tab eventKey="third" title="Relation Extraction">
            <br></br>
            <Card >
              <br></br>
              <div>
                {list.relation.map(obj => {
                  return (
                    <div>
                      <p>SujectId - {obj.subjectId}</p>
                    </div>
                  );
                })}

              </div>
            </Card>

          </Tab>
          <Tab eventKey="fourth" title="Json">
            <br></br>
            <Card >
              <br></br>
              <button style={{ marginLeft: "60%" }}
                type="button" onClick={exportData}>
                Download Json response
              </button>
              <br></br>
              <div>
                <p>
                  <pre>
                    {(JSON.stringify(list.fulljson, null, 2))}
                  </pre>
                </p>
              </div>



            </Card>

          </Tab>
        </Tabs> */}
        {/* </Card.Body> */}


      </div >
    </Card>

  )
}

export default HealthNLPAPI;








