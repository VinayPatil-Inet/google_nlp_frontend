import React, { useEffect, useRef, useState } from 'react';
import { Form, Button, Card, Alert } from 'react-bootstrap';
import { useAuth } from '../contexts/AuthContext';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { async } from '@firebase/util';
import { Redirect } from "react-router-dom";
import Img from './BCBSRI-LOGO.png'
import { set, useForm } from "react-hook-form";
import axios from 'axios';
//export const BASE_URL = process.env.REACT_APP_BASE_URL;


const Login = () => {
    const INITIAL = {
        email: "",
        password: ""

    }


    var url = process.env.REACT_APP_BASE_URL+'/api/user/login';
    console.log(url, "url=====");
    const [data, setData] = useState(INITIAL);
    const [message, setMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = React.useState("");

    const history = useHistory();


    function handleChange(e) {
        const { id, value } = e.target;
        setData({ ...data, [id]: value })
    }
    function handleSubmit(e) {
        e.preventDefault();

        axios.post(url, data)
            .then(res => {

                history.push('/getData');

            })
            .catch(error => {

                if (error.response && error.response.data) {
                    var message = error.response.data.message
                    console.log(error.response.data.message, "error message")
                    setErrorMessage(message)
                    setTimeout(() => {
                        setErrorMessage()
                    }, 2000);
                }

                console.log(error.data, "error");
            })


    }

    return (

        <div style={{ width: '30%', marginLeft: '35%', marginTop: "5%" }}>
            <Card>
                <Card.Body>
                <center>
            <img src={Img} alt="pic" width={200} /></center>
          <hr></hr>

                    <h2 className='text-center mb-4 '>Log In</h2>
                    <strong> {successMessage && <div className="d-flex justify-content-center success" style={{ color: "green", paddingBottom: "5px", paddingTop: "5px", textAlign: "center" }} > {successMessage}</div>} </strong>
                    <strong> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red", paddingBottom: "5px", paddingTop: "5px", textAlign: "center" }} > {errorMessage}</div>} </strong>

                    <form onSubmit={handleSubmit} >

                        <Form.Group className="mb-3" controlId="pcform.email">
                            <Form.Label>Email</Form.Label>
                            <input className="form-control" type="text" name="email" id="email"
                                onChange={handleChange}
                                value={data.email}

                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="pcform.password">
                            <Form.Label>Password</Form.Label>
                            <input className="form-control"
                                name="password" id="password" type='password'
                                onChange={handleChange}
                                value={data.password}
                            />
                        </Form.Group>
                        <div className="w-100 text-center mt-3">
              <strong style={{ marginLeft: "65%" }}> <Link to='/ForgotPassword'>Forgot Password</Link></strong>
            </div>

                        <center><Button type='submit'
                         disabled={!data.email || !data.password}
                            className='w-90 mt-4'>Submit</Button></center>


                    </form>


                </Card.Body>
            </Card>

        </div>

    )



}

export default Login;








