import React from "react";
//import classes from "./Spinner.module.css";

import  classes from  "../Spinner/Spinner.Module.css"

const Spinner = () => {
  return <div className={classes.Loader}>Loading...</div>;
};

export default Spinner;