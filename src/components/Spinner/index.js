import React from "react";
import styled from "styled-components";

const Spinner = styled.div`
  padding: 6px 12px;
  border-radius: 4px;
  border: 1px solid dodgerblue;
  display: inline-block;
  color: dodgerblue;
`;

export default () => <Spinner>Loading...</Spinner>;
